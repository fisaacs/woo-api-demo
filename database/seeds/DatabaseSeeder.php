<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');

        DB::table('stores')->insert([
            [
                'name' => "Demo Store",
                'url' => 'http://couture-demo-store.isaacsdigital.com/',
                'description' => 'Main MU Site',
                'client_key' => 'ck_272fc0a5b89a45477f7a1c3656f311acd399d997',
                'client_secret' => 'cs_60dc7b1bbe1b410b9116e1d3f493d89f5a0809ea'
            ],
            [
                'name' => "Fresh Dressed",
                'url' => 'http://couture-demo-store.isaacsdigital.com/clothing',
                'description' => 'Sub MU Site',
                'client_key' => 'ck_7d57b968cd2f37cdf60b833bf8c5f6831a576ffc',
                'client_secret' => 'cs_2b6b47e9945aa50af4a598fc159326587fa3a059'
            ]
        ]);
    }

}
