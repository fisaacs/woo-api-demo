<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->text('description')->nullable();
            $table->string('client_key');
            $table->string('client_secret');
            $table->timestamps();
        });

        Schema::create('products', function(Blueprint $table){
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('name');
            $table->text('data')->nullable();
            $table->timestamps();

            $table->foreign("store_id")->references('id')->on("stores");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
        Schema::drop('stores');

    }
}
