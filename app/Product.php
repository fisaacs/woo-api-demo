<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 3/14/17
 * Time: 9:43 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $fillable = [
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function setDataAttribute($value) {
        $this->attributes["data"] = json_encode($value);
    }

    public function wooStore() {
        return $this->belongsto('App\Store');
    }
}