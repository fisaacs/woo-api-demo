<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 3/14/17
 * Time: 7:46 PM
 */

namespace App\Http\Controllers;


use App\Product;
use App\Store;
use Automattic\WooCommerce\Client;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        return response()->json(Store::all());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            "client_key" => "required",
            "client_secret" => "required"
        ]);

        $store = Store::create($request->only('name', 'url', 'description', 'client_key', 'client_secret'));
        $store->save();
        return response()->json($store);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $store = Store::find($id);
        $store->fill($request->only('name','url', 'description', 'client_key', 'client_secret'));
        $store->save();
        return response()->json($store);

    }

    public function destroy($id)
    {
        $store = Store::find($id);
        $store->delete();
        return response();
    }

    public function products($id, Request $request) {

        $store = Store::find($id);
        return response()->json($store->products()->get());

//        return response()->json($store);
    }


    protected function consumeProducts(Store $store) {
        $woo =  new Client(
            $store->url,
            $store->client_key,
            $store->secret_key,
            [
                'version' => 'v3',
            ]
        );

        $data = $woo->get('products');
        foreach($data["products"] as $row) {
            $product = new Product();
            $product->product_id = $row["id"];
            $product->name = $row["title"];
            $product->setFeedData($row);
            $product->Store()->save($store);
            $product->save();
        }
    }

    public function testWooCommerce() {
        $woocommerce = new Client(
            'http://demo-store.app',
            'ck_272fc0a5b89a45477f7a1c3656f311acd399d997',
            'cs_60dc7b1bbe1b410b9116e1d3f493d89f5a0809ea',
            [
                'version' => 'v3',
            ]
        );

        $data = $woocommerce->get('products');

        echo "<pre>";
        var_dump($data["products"][0]["title"]);
        echo "</pre>";
        exit;
    }

}