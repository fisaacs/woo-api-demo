<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 3/14/17
 * Time: 9:42 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public $table = "stores";

    public $fillable = [
        "name", "url", "description", "client_key", "client_secret"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products() {
       return $this->hasMany('App\Product');
    }

}