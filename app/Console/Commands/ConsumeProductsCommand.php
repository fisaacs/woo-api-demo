<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 3/14/17
 * Time: 11:29 PM
 */

namespace App\Console\Commands;

use App\Product;
use App\Store;
use Automattic\WooCommerce\Client;
use Illuminate\Console\Command;

class ConsumeProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate products table from saved WooCommerce Stores';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = Store::all();
        foreach($stores as $store) {
            var_dump($store->id);
            $woo =  new Client(
                $store->url,
                $store->client_key,
                $store->client_secret,
                [
                    'version' => 'v3',
                ]
            );

            $data = $woo->get('products');
            foreach($data["products"] as $row) {
                $product = new Product();
                $product->product_id = $row["id"];
                $product->name = $row["title"];
                $product->data = $row;
                $store->products()->save($product);
            }
        }
    }

}