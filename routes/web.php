<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('home');
});

$app->get('/test', 'StoreController@testWooCommerce');

$app->get('/api/stores', 'StoreController@index');
$app->get('/api/stores/{id}', 'StoreController@show');
$app->put('/api/stores', 'StoreController@store');
$app->post('/api/stores/{id}', 'StoreController@update');
$app->delete('/api/stores/{id}', 'StoreController@delete');
$app->get('/api/stores/{id}/products', 'StoreController@products');